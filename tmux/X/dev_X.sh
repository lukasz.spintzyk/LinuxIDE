#!/bin/bash
set -e
set -u

function setup_workspace()
{
  echo "Download XServer sources"
  git clone https://gitlab.freedesktop.org/xorg/xserver.git
  (cd xserver;
  git remote add x-debian  https://salsa.debian.org/xorg-team/xserver/xorg-server.git
  git remote add lspintzyk.free https://oauth2:-_cCc6frDj3YLwdEoz25@gitlab.freedesktop.org/Spintzyk/xserver.git
  ctags -R .
  )

  echo "Download xorg proto source"
  git clone https://gitlab.freedesktop.org/xorg/proto/xorgproto.git
  (cd xorgproto
  ctags -R .
  )

  echo "Download amd ddx driver"
  git clone https://gitlab.freedesktop.org/xorg/driver/xf86-video-amdgpu.git
  (cd xf86-video-amdgpu
  git remote add x-debian https://salsa.debian.org/xorg-team/driver/xserver-xorg-video-amdgpu.git
  ctags -R .
  )

  echo "Download ati ddx driver"
  git clone https://gitlab.freedesktop.org/xorg/driver/xf86-video-ati.git
  (cd xf86-video-ati
  ctags -R .
  )

  echo "Download r128 ddx driver"
  git clone https://gitlab.freedesktop.org/xorg/driver/xf86-video-r128.git
  (cd xf86-video-r128
  ctags -R .
  )

  echo "Download nouveau ddx driver"
  git clone https://gitlab.freedesktop.org/xorg/driver/xf86-video-nouveau.git
  (cd xf86-video-nouveau
  ctags -R .
  )

  echo "Download igt-tools"
  git clone https://gitlab.freedesktop.org/drm/igt-gpu-tools.git
  (cd igt-gpu-tools
  ctags -R .)

  echo "Download libdrm from mesa"
  git clone https://gitlab.freedesktop.org/mesa/drm.git
  (cd drm
  ctags -R .
  )

  echo "Download mesa graphics library"
  git clone https://gitlab.freedesktop.org/mesa/mesa.git
  (cd mesa
  ctags -R .)

  echo "Download piglit - opengl test framework"
  git clone https://gitlab.freedesktop.org/mesa/piglit.git
  (cd piglit
  ctags -R .)

  echo "Download linux kernel"
  git clone https://github.com/torvalds/linux.git

  echo "Clone evdi repository"
  git clone https://github.com/DisplayLink/evdi
  (cd evdi
  ctags -R .
  )
}

function build_all()
{
  echo "Build XServer sources"
  (cd xserver;
  bash autogen.sh
  make
  )

  echo "Build amd ddx driver"
  (cd xf86-video-amdgpu
  )

  echo "Build ati ddx driver"
  (cd xf86-video-ati
  )

  echo "Build r128 ddx driver"
  (cd xf86-video-r128
  )

  echo "Build nouveau ddx driver"
  (cd xf86-video-nouveau
  )

  echo "Build igt-tools"
  (cd igt-gpu-tools
  meson -Drunner=enabled build && ninja -C build)

  echo "Build libdrm from mesa"
  (cd drm
  meson builddir
  ninja -C builddir/
  )

  echo "Build mesa graphics library"
  (cd mesa
  meson builddir
  ninja -C builddir)

  echo "Build piglit - opengl test framework"
  (cd piglit
  cmake .
  make)

  echo "Build evdi repository"
  (cd evdi
  make)
}


mkdir -p ~/xworkspace
(cd ~/xworkspace
setup_workspace
build_all
)
