#!/bin/bash

SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")

tmux -f $SCRIPT_DIR/xworkspace.conf attach
