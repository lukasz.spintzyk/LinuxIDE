#!/bin/bash

pacman -S --noconfirm extra/vim
pacman -S --noconfirm extra/gvim
pacman -S --noconfirm extra/git
pacman -S --noconfirm community/tig
pacman -S --noconfirm core/automake
pacman -S --noconfirm core/autoconf
pacman -S --noconfirm core/gcc
pacman -S --noconfirm core/make
pacman -S --noconfirm core/gdb
pacman -S --noconfirm extra/gdb
pacman -S --noconfirm extra/subversion
pacman -S --noconfirm extra/scons
pacman -S --noconfirm community/python2
pacman -S --noconfirm community/python
pacman -S --noconfirm extra/python2
pacman -S --noconfirm extra/boost
pacman -S --noconfirm core/patch
pacman -S --noconfirm extra/xterm
pacman -S --noconfirm extra/python2-pip
pacman -S --noconfirm extra/python2-chardet
pacman -S --noconfirm community/patchutils
pacman -S --noconfirm extra/expect
pacman -S --noconfirm dos2unix
