#!/bin/bash
set -u
set -e

function isMac()
{
  [[ $(uname) == "Darwin" ]]
}

function isLinux()
{
  [[ $(uname) == "Linux" ]]
}

function isDebian()
{
  if isLinux; then
    if [ "$(lsb_release -is)" == "Debian" ]; then
      return 0
    fi
  fi
  return 1
}

function isRaspbian()
{
  if isLinux; then
    if [ "$(lsb_release -is)" == "Raspbian" ]; then
      return 0
    fi
  fi
  return 1
}

function isUbuntu()
{
  if isLinux; then
    if [ "$(lsb_release -is)" == "Ubuntu" ]; then
      return 0
    fi
  fi
  return 1
}


function isDebianLike()
{
  if isDebian; then
    return 0
  elif isRaspbian; then
    return 0
  elif isUbuntu; then
    return 0
  fi
  return 1
}

function yes_no_prompt()
{
  local question="$1 (y/n)? "
  local answer
  read -p "$question" answer
  case ${answer:0:1} in
    y|Y )
        return 0
    ;;
    * )
        return 1
    ;;
  esac
}

function download()
{
  local install_dir=$1
  local git_url="https://gitlab.com/lukasz.spintzyk/LinuxIDE.git"

  if [ -d "$install_dir" ]; then
    return
  fi
  git clone $git_url $install_dir
}

function setup_git()
{
  local scope_param=${1:-}
  local gitusername
  local gituseremail
  if [ "$scope_param" == "--global" ]; then
    echo "Set git global account"
  else
    echo "Set git repo account"
  fi

  read -p 'Git username: ' gitusername
  read -p 'Git useremail: ' gituseremail

  git config --global user.name "$gitusername"
  git config --global user.email "$gituseremail"
  git config --global core.editor vim
}

function install_console_fonts()
{
  local console_dir=$install_dir/console-setup

  if isDebianLike; then
    sudo cp $console_dir/console-setup-default /etc/default/console-setup
    sudo cp $console_dir/keyboard-default /etc/default/keyboard
    sudo setupcon --save

    ln -s $console_dir/console-setup-local ~/.console-setup
    ln -s $console_dir/keyboard-local ~/.keyboard
    setupcon --save
    echo "setupcon" >> ~/.bashrc
  fi
}

function install_powerline()
{
  if isDebianLike; then
    if isUbuntu; then
      sudo apt install -y powerline fonts-powerline
    else
      sudo apt install -y powerline powerline-gitstatus fonts-powerline
    fi
cat >> ~/.bashrc << EOL
if [ -f `which powerline-daemon` ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTINUATION=1
  POWERLINE_BASH_SELECT=1
fi
if [ -f /usr/share/powerline/bindings/bash/powerline.sh ]; then
  source /usr/share/powerline/bindings/bash/powerline.sh
fi
export TERM="screen-256color"
EOL
  elif isMac; then
    pip3 install powerline-status
    pip3 install powerline-gitstatus
cat >> ~/.bashrc << EOL
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
source /usr/local/lib/python3.7/site-packages/powerline/bindings/bash/powerline.sh
EOL

cat >> ~/.bash_profile << EOL
powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
source /usr/local/lib/python3.7/site-packages/powerline/bindings/bash/powerline.sh
EOL

    mkdir ~/.config/powerline
    cp -r /usr/local/lib/python3.7/site-packages/powerline/config_files/ ~/.config/powerline/
  fi
}

function install_neofetch()
{
  if isUbuntu; then
    sudo add-apt-repository ppa:dawidd0811/neofetch
    sudo apt update || true
  fi
  if isDebianLike; then
    sudo apt install -y neofetch
  elif isMac; then
    brew install neofetch
    echo "neofetch" >> ~/.bash_profile
  fi
  echo "neofetch" >> ~/.bashrc
}

function install_tmux()
{
  local install_dir=$1

  if isDebianLike; then
    sudo apt install -y tmux
  elif isMac; then
    brew install tmux
    echo "source /usr/local/lib/python3.7/site-packages/powerline/bindings/tmux/powerline.conf"
  fi
  ln -s $install_dir/tmux/tmux.conf ~/.tmux.conf
}

function install_alacritty()
{
  if isRaspbian; then
    return 0
  fi

  if isDebianLike; then
    (cd /tmp
      wget -O Alacritty.dpkg https://github.com/alacritty/alacritty/releases/download/v0.4.1/Alacritty-v0.4.1-ubuntu_18_04_amd64.deb
      sudo dpkg -i Alacritty.dpkg
    )
  elif isMac; then
    (cd $TMPDIR
      wget -O Alacritty.dmg https://github.com/alacritty/alacritty/releases/download/v0.4.1/Alacritty-v0.4.1.dmg
      open Alacritty.dmg
    )
  fi
  mkdir -p ~/.config/alacritty
  local alacritty_settings=~/.config/alacritty/alacritty.yml
  cp $install_dir/alacritty/material_theme.yml $alacritty_settings
  if isMac; then
    echo "key_bindings:" >> $alacritty_settings
    echo "  - { key: N,         mods: Command,      action: SpawnNewInstance       }" >> $alacritty_settings
  fi
}

function install_dependencies()
{
  if isMac; then
    brew install wget
    brew install python3
    brew install ctags
  elif isDebianLike; then
    sudo apt install vim git tig openssh-server wget -y
    sudo apt install exuberant-ctags -y
  fi
}

function install()
{
  local install_dir=$1

  install_dependencies
  if yes_no_prompt "install vim configuration"; then
    setup_vim $install_dir
  fi

  if yes_no_prompt "Setup global git account"; then
    setup_git "--global"
  fi

  if yes_no_prompt "Setup powerline"; then
    install_powerline
  fi

  if yes_no_prompt "Install tmux"; then
    install_tmux $install_dir
  fi

  if yes_no_prompt "Setup console-fonts"; then
    install_console_fonts $install_dir
  fi

  if yes_no_prompt "Setup neofetch"; then
    install_neofetch
  fi

  if yes_no_prompt "Install Alacritty"; then
    install_alacritty
  fi

}

function setup_vim()
{
  local install_dir=$1
  echo "Setup vim"

  rm -rf ~/.vim
  rm -f ~/.vimrc

  ln -s $install_dir/vim ~/.vim
  ln -s ~/.vim/vimrc ~/.vimrc
}

INSTALL_DIR=$(pwd)
download $INSTALL_DIR
install $INSTALL_DIR


